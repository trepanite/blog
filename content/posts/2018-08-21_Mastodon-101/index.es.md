---
title: "Guía de inicio rápido de Mastodon"
description: "Así que quieres unirte a Mastodon y empezar a tootear. ¡Genial! Aquí las instrucciones para entrarle directo."
date: 2018-08-27
draft: false
author: nico
resources:
- name: hero
  src: vlcsnap-2018-08-27-16h43m11s127.png
categories:
- Guides
tags:
- mastodon
---

Así que quieres unirte a Mastodon y empezar a tootear. ¡Genial! Aquí las instrucciones para entrarle directo.

### Empecemos con lo básico. ¿Qué es esto?

Mastodon es una plataforma de microblogging parecida a otras que habrás visto, como Twitter, pero en lugar de estar centralizada es una red federada que funciona de forma similar al correo electrónico.

Como con el correo electrónico, eliges un servidor y te registras. Ya sea Gmail, Outlook, iCloud o cualquier otro, sabes que podrás enviar correos a todas las personas que quieras con solo saber su dirección.

> En Mastodon es común que usemos la palabra “instancia” como sinónimo de servidor.

Esto significa que no hay una corporación enorme y descuidada al mando, ni accionistas, ni un control centralizado, ni los contenidos patrocinados de los que estamos hartos; solo un grupo de personas compartiendo lo que quieren compartir entre sí.

### ¿Dónde me registro?

Lo primero que debes hacer es **escoger tu instancia**. Este es un paso extra comparado con sitios como Twitter o Tumblr, pero es bastante simple.

> Como con el correo, tu identidad es hospedada por el servidor donde te has registrado. Por ejemplo, si yo me uní a la instancia mastodon.social, entonces para mencionarme deberás escribir @nico@mastodon.social en tu publicación.

Si de lo que más te interesaría hablar entra claramente dentro de una categoría (tal vez videojuegos, o arte, o sexodiversidad, o programación, o literatura, o lo que sea) entonces quizás valga la pena que tu primera instancia sea una que aloje principalmente ese tipo de contenido –será más fácil entrar en contacto y encontrar amigxs–. Algunxs tomarán su servidor como si fuera un vecindario o un espacio abierto, donde la conversación general puede tener un tema específico.

> Puedes ver todas las publicaciones públicas –valga la redundancia– “locales” que hagan los otros participantes de tu instancia en la llamada “cronología local”.

Si no tienes la intención de apegarte a un solo tema, será mejor que elijas una instancia de temas generales. De cualquier modo, hay una herramienta muy práctica para elegir servidor en [joinmastodon.org](https://joinmastodon.org/#getting-started)

**¡No desesperes!** Igual podrás conversar con gente de otras instancias, sin importar cuál elijas para registrarte. Recuerda que esto es como el correo electrónico: desde tu cuenta de Gmail puedes escribirle a tu mamá a su milenaria cuenta de Yahoo, por ejemplo.

> La palabra “fediverso” (universo federado) se refiere a la red de todos los servidores de Mastodon [y otros proyectos]({{< ref "posts/2018-06-27_Why-ActivityPub-is-the-future/index.es.md" >}}), cuyxs usuarixs perfectamente pueden comunicarse entre sí.

Con el tiempo, puede que empieces a querer tener otra cuenta en un servidor diferente, ya sea trasladando tu cuenta principal o abriendo una nueva para algún aspecto particular de ti mismx. Esto es algo común en el fediverso y nada por lo que preocuparse. La gente está habituada a ver una publicación esporádica como esta:

{{< mastodon "https://mastodon.social/@JotaSeth/103392903535486614" >}}

### Conociendo a tu servidor

Antes de registrarte, dedica un momento para revisar las reglas de la instancia que elegiste y asegúrate de que sean compatibles con la forma en la que quieres tootear.

> Las publicaciones en Mastodon se llaman “toots”. Porque ese sonido hacen los elefantes.

Debajo del formulario de registro encontrarás un enlace a la página con el reglamento. Es probable que esté enlazada en el botón “Saber más” bajo “Administrado por”. En otras páginas, las reglas están enlazadas en el pie de la página como un simple “Acerca de”. También puedes introducir la URL correcta directamente en la barra de direcciones de tu navegador, ya que siempre tiene un formato como `https://mastodon.la/about/more`.

La página con las reglas también te dirá quién es admin/propietarix de la instancia. La mayoría de los servidores están configurados para que por defecto sigas al admin cuando te registras. Esto es genial: significa que sabrás de inmediato a quién consultar si tienes dificultades y podrás recibir anuncios específicos del servidor (como cuando el software está siendo actualizado) y, en general, es importante saber quién es responsable de la instancia en la que estás registradx.

Lxs administradorxs son personas súper amigables que generalmente mantienen el servidor con dinero de su bolsillo, de modo que será bueno llegar a conocerlos tan bien como conocerías a tu arrendatarix. Muchos aceptan donaciones para cubrir los costos de mantenimiento y, siempre que puedas, tu contribución será apreciada.

### ¡Creo que encontré un nuevo hogar!

Ve a la página de inicio de tu instancia e introduce tu nombre de usuarix y contraseña en el formulario de registro. Necesitarás una cuenta de correo y se te solicitará que la confirmes antes de iniciar sesión por primera vez.

Lo siguiente es subir tu imagen de perfil y echar un vistazo a la página de configuración (asegúrate de regresar cuando ya hayas estado en Mastodon unos días para hacer cualquier ajuste que pueda mejorar tu experiencia). Luego, prepárate para presentarte.

> Algunos ajustes interesantes que cabe mencionar son: la autenticación de dos factores (2FA) para mejorar la seguridad de tu cuenta; la reproducción automática de GIFs (desactivada por defecto); el idioma en el que piensas publicar; y los idiomas que prefieres ver en las cronologías local, federada y de etiquetas (por defecto verás todos los idiomas).

Las etiquetas son realmente importantes en Mastodon. De hecho, son la [única parte del contenido de las publicaciones que funciona en las búsquedas]({{< ref "posts/2018-07-05_Cage-the-Mastodon/index.es.md" >}}). Si quieres que te encuentre gente que busca publicaciones sobre fotografía será mejor que incluyas #fotografía.

> Por razones de accesibilidad, para etiquetas de múltiples palabras por favor utiliza mayúsculas y minúsculas #ComoEstaBuenaEtiqueta en vez de #estamalaetiqueta.

Para tu primer “tut”, es buena idea publicar una breve #introducción con información acerca de ti, tus intereses y los temas que te gustaría abordar en Mastodon. Esa también es una excelente etiqueta para buscar: encontrarás montones de personas nuevas en la red como tú y a muchas les gustarán las mismas cosas que a ti te gustan.

{{< mastodon "https://mstdn.mx/@ricardito/101570566332371202" >}}

{{< mastodon "https://social.coop/@tierra_comun/100341976558504965" >}}

{{< mastodon "https://mastodon.lat/@Quesadilla/100664291145725888" >}}

### Un paseo rápido por la interfaz web

> Mastodon soporta [varias aplicaciones](https://joinmastodon.org/apps), tanto para móviles como para escritorio: no estás atado a la interfaz estándar. Si buscas un entorno súper simple, puedes probar [Pinafore](https://pinafore.social).

{{< figure src="default-interface.png" caption="Esquema de la interfaz de usuarix por defecto" >}}

La interfaz estándar de Mastodon tiene múltiples columnas en vez un solo hilo. Puedes moverlas, quitarlas u ordenarlas como prefieras.

**Inicio** son todos los tuts, en orden cronológico, de la gente que sigues. Incluye publicaciones de personas tanto de tu instancia como de las demás, lo único importante es que es gente a la que sigues. Hay quien prefiere desactivar los boosts en esta columna para ver únicamente lo que publican lxs usuarixs que sigue. La opción está en el botón de ajustes, en la esquina superior derecha de la columna.

> “Boost” (como en “signal boost” o “rocket boost”: impulso extra) es un sinónimo de “reblog” o “retuit” en Mastodon.

**Notificaciones** hace lo que dice en la imagen: es lo que otros te envían. Otra vez, esto es a través del fediverso. El botón de ajustes (arriba a la derecha) tiene una serie de opciones para esta columna. Es posible, por ejemplo, que quieras apagar el sonido “boop”.

**Cronología local** es el hilo en vivo con los toots de las personas de tu misma instancia. En muchos servidores, particularmente los pequeños y los que se enfocan en un tópico particular, aquí es donde ocurre la magia. Es como la plaza del pueblo o una sala de chat en los 2000. Puedes responder a la gente desde allí mismo y es un excelente lugar para conocer personas.

**Cronología federada** muestra toots de toda la red conocida por tu instancia (incluidos los toots locales de todas las instancias públicas). El motivo más común para que un toot aparezca aquí es que sea de alguien a quien otrx usuarix de tu instancia sigue. Esta columna se mueve rápido, y con frecuencia puede ser bastante salvaje. Personalmente, me divierte configurarla para mostrar solo toots con medios y ocultar los boosts, y entonces veo un flujo constante de selfies tontas, memes virales y extravagancias artísticas.

También puedes dedicar una columna a una etiqueta que te interesa (solo busca esa etiqueta y en los ajustes de la columna elige “fijar” y listo).

### Uso de las alertas de contenido

Una de las mejores características de Mastodon es el botón que dice “CW”, donde escribes tus toots. Tocarlo añade un campo de alerta de contenido en el que puedes poner información sobre el contenido del toot (p. ej. salud mental, política, lenguaje obsceno, desnudez) para que la gente no tenga que ver algo que, por la razón que sea, desea evitar. Desde luego, también es genial para ocultar spoilers.

Una convención general es poner +, -, o ~ en una advertencia de contenido para señalar si los contenidos son mayormente positivos, negativos, o una mezcla, respectivamente.

Mi consejo es simple: si dudas de que un toot necesite o no un CW, ponle un CW. La gente en verdad lo aprecia y no hace ningún daño ser por demás cautelosos y respetuosos con lxs otrxs.

También puedes usar una AC para resumir una publicación larga. Hay quien las usa para rematar chistes. Tal vez se te ocurran otros usos. Diviértete.

### Aprende más

Material Oficial:

* [¿Qué es Mastodon?](https://peertube.tv/videos/watch/887e7b95-a6b1-4df1-83fd-d5c47f17ec4c)
* [Documentation](https://docs.joinmastodon.org/)
* [Apps para iOS and Android](https://joinmastodon.org/apps)
* [How to become a successful artist on Mastodon]({{< ref "posts/2018-06-24_How-to-become-a-successful-artist-on-Mastodon/index.md" >}})

Material Comunitario:

* [Intro to Mastodon - Tahajin](https://peertube.tv/videos/watch/23d3e2dd-b91a-45f8-82d7-383d0341f6f1)
* [A Beginner's Guide to Mastodon - Lifehacker](https://lifehacker.com/a-beginner-s-guide-to-mastodon-1828503235)
* [An Increasingly Less-Brief Guide to Mastodon - Joyeuse Noelle](https://github.com/joyeusenoelle/GuideToMastodon/)
