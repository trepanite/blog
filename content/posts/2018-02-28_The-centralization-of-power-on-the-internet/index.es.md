---
title: "La centralización del poder en internet"
date: 2018-02-28
draft: false
author: mainebot
resources:
- name: hero
  src: 1_qEhCKxSQMf1NFACKmxjTkQ.jpeg
categories:
- Op-Ed
tags:
- mastodon
- amazon
- facebook
- twitter
---

El espacio en internet está dominado por un pequeño grupo de compañías con un poder y una influencia desproporcionados sobre toda la experiencia en línea, no solo en las redes sociales. Una influencia tal que varias de estas compañías alteraron muchos aspectos fundamentales de la vida fuera de internet; cambios generalmente descriptos con el léxico florido de los privilegiados como “disruptivos”, pero más claramente comprensibles en lenguaje coloquial: destructivos.

Las cinco compañías más valiosas a finales de 2017 eran, en orden: Apple, Alphabet (el conglomerado propietario de Google), Microsoft, Amazon y Facebook. Cada una no solo depende, sino que está al mando de grandes porciones del paisaje tecnológico. ¿Qué tienen todas estas empresas en común?

* El valor de cada una supera el medio *billón* de dólares;
* No describen la vida en línea, la dictan;
* Hacen esfuerzos extremos por mantener un ecosistema cerrado;
* Son estructuras de poder monolíticas y centralizadas, con una influencia inimaginable.

Todas estas empresas intentan dominar el lugar metafórico que generalmente denomino “el último kilómetro”. En logística, el último kilómetro es la distancia entre el centro de distribución y tu puerta, pero el término también se puede aplicar al espacio entre el contenido en línea y tu dispositivo. Si un sitio publica noticias, o videos, o cualquier clase de información digital, estas son las empresas que trabajan para obligarlo a pasar a través de un portal que poseen en lugar de permitir que tú, como usuarix, abandones su plataforma y vayas a otro sitio.

El control sobre este último kilómetro debería estar en poder de la gente, y no centralizado bajo una estructura corporativa. Imagina una experiencia de internet donde nunca tuvieras permitido salir del cerco de Facebook, o no pudieras ver una película, un video o ni siquiera una foto, fuera de un sitio con el logo ubicuo de Google en un rincón.

[En un artículo reciente de Splitsider](http://splitsider.com/2018/02/how-facebook-is-killing-comedy/) (en inglés), Sarah Aswell conversa con Matt Klinman acerca del nocivo efecto de Facebook sobre el humor en línea y el problema que en cierto sentido ha causado con todos los formatos a medida que surgían en internet. Puedes ir a leerlo, yo esperaré aquí.

El intento de Facebook de canalizar todas las posibilidades de la red a través de su iniciativa [internet.org](https://info.internet.org/) y sus asociaciones colaborativas locales es una forma directa de negar a lxs ciudadanxs de países emergentes aquella suerte de espacio libre, autoregulado y desprolijo, que dimos por garantizado en internet y estamos perdiendo rápidamente. Imagina que más de la mitad de la población mundial jamás experimentara un internet de posibilidades inciertas, de voces diferentes, de expresión libre, de sorpresas, hallazgos y descubrimientos, un internet que no haya sido diseñado para estar bajo el absoluto control de Facebook (incluida su evidente necesidad de controlar el flujo completo de publicación de contenidos, de monetizar todos los aspectos de la experiencia y de forzar al usuario a ver publicidad constantemente).

Consideremos lo que dijo Klinman:


> "En esencia, Facebook practica una estafa payola donde incluso si quieres que tus propixs seguidorxs vean tu contenido, tienes que pagar. Si diriges una gran compañía editorial y produces una excelente pieza de contenido de la cual estás orgullosx, la publicas en Facebook. De ahí en adelante su algoritmo toma el control, sin la menor transparencia. Así, no solo el sitio web deja de recibir los ingresos que antes recibía por publicidad, sino que ahora además debe pagar a Facebook para que entregue el contenido a sus propixs seguidorxs. Por lo tanto, Facebook se apropia no solo de las ganancias por colocar publicidad en lo que sea que lxs usuarixs estén viendo, sino también del pago que exige a lxs editorxs. Es como si además de la suscripción al periódico tuvieras que pagarle al repartidor por cada artículo que quieres leer".

Pienso en Amazon, y su intento de controlar la experiencia mercantil, comercial.

{{< figure src="1_1_pQeoTAbj_-uux6bZaOBg.jpeg" >}}

Considera cada tienda virtual en Amazon: son idénticas en muchos aspectos, con poco y nada que permita a una diferenciarse de otra. Los únicos detalles destacados son: el costo del producto, los costos de envío, y si está disponible en prime. Homogeneiza toda la experiencia de compra en línea y empuja a todo el mundo a comprar en un solo sitio. Una vez que se consolida como el único espacio razonable para comprar en línea, toma el control completo sobre lxs vendedorxs y su capacidad de vender, y puede cobrar arbitrariamente a la gente por permitirle participar en su espacio. Tal como Facebook y lxs editorxs de contenido.

La presión de Amazon por dominar el último kilómetro se debe a que con ello obtendría el control de la cadena comercial completa: quién puede vender, quién puede ver los productos y cuándo llegan. Arruina a la competencia en la distribución y privatiza cada etapa con una única marca. Si quieres competir en el mercado, tienes que bajar los precios hasta el mínimo para sobrevivir o serás eliminado. La meta de Amazon, como la de Facebook, es conquistar absolutamente el espacio y eliminar toda posibilidad de competencia.

Incluso en el pasado reciente, puedes ver este patrón siendo implementado una y otra vez. Amazon compra Whole Foods para controlar un gran segmento de espacios de góndola en el comercio de víveres. Las redes sociales alternativas como Instagram, Whatsapp, Periscope y otras son absorbidas e incorporadas a una interfaz únificada; aplanadas de actualización en actualización hasta disolverlas en una experiencia gris, monótona y homogénea, sin ninguna diferencia apreciable con la de la compañía que las compró.

El control centralizado elimina el poder de elegir, y lo reemplaza por una ilusión de selección.

Mastodon es un primer paso importante para permitir que la gente se reapropie de sus canales de interacción. Otorga a todxs la oportunidad de diversificar su universo social en línea, y evitar que el dinero sea el único factor para decidir quién puede ver, oír o decir cualquier cosa en internet.

*¡Para empezar con Mastodon, visita [JoinMastodon.org](https://joinmastodon.org/) y escoge un lugar al que llamar hogar! ¡Usa los menús desplegables para afinar tu búsqueda por intereses e idioma, y encuentra una comunidad que sientas como propia! ¡No dejes que el fediverso se pierda lo que tienes para decir!*
