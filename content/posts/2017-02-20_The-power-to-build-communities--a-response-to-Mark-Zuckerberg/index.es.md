---
title: "El poder de construir comunidades"
subtitle: "Una respuesta a Mark Zuckerberg"
date: 2017-02-20
draft: false
author: gargron
categories:
- Op-Ed
tags:
- mastodon
- facebook
---

El manifiesto de Mark Zuckerberg podrá ser bienintencionado, pero hay una cosa fundamentalmente equivocada en él:

> En tiempo como éstos, lo más importante que podemos hacer en Facebook es desarrollar la infraestructura social para darle a la gente el poder de construir una comunidad global que funcione para todos nosotros.

Facebook no es, ni podrá ser nunca, una plataforma donde la gente tenga el poder de construir nada. Facebook ni siquiera tiene las pretensiones de ser una organización sin fines de lucro como Wikipedia o Mozilla. No hay ninguna duda sobre el objetivo principal de la empresa: extraer de ti tanto como sea posible (mediante el análisis de datos y la exhibición de avisos a cambio del dinero de los anunciantes). Un futuro donde Facebook es la infraestructura social global es un futuro donde no hay refugio de la publicidad y el cálculo numérico.

Facebook no puede darle a nadie el poder para hacer nada simplemente porque esa voluntad de poder siempre residirá, en definitiva, en Facebook mismo, que controla tanto el software como los servidores y las políticas de moderación.

No, el futuro de las redes sociales debe ser la federación. Su mayor poder reside en otorgar al pueblo la capacidad de crear sus propios espacios y organizar sus propias comunidades, de modificar el software como lo consideren más conveniente, pero sin sacrificar la posibilidad de que lxs miembrxs de comunidades diferentes puedan interactuar entre sí. Desde luego, no todxs lxs usuarixs estarán interesados en administrar su pequeña red social propia (así como no todxs lxs ciudadanxs están interesadxs en gobernar su propio pequeño país). Pero pienso que hay una buena razón por la que muchos países se organizan en estados o provincias separados pero compatibles, y por la que muchos países separados pero compatibles construyen alianzas como la CELAC o la Unión Europea: el equilibrio entre unidad y soberanía. La federación.

Internet ha visto el surgimiento y la caída de muchas plataformas sociales. MySpace. Friendfeed. Google+. App.net. Una y otra vez: adaptarse a diferentes entornos de uso, abrir nuevas cuentas, convencer a nuestrxs amigxs de que se cambien de plataforma o tener que administrar varias cuentas para mantenernos en contacto. ¿Crees que este ciclo se detendrá con Facebook? Las dinámicas comunitarias garantizan en cierta medida un ciclo de esplendor y decadencia, pero podríamos dejar de arrastrarnos mutuamente de plataforma en plataforma y apegarnos a un protocolo estandarizado. Tal vez el correo electrónico no sea muy sensual, porque fue creado en una época más simple, pero es imposible negar que aún funciona, sin importar qué proveedor elijas.

¿Quieres que a cargo de la comunidad global esté el sitio que te muestra fotos de tus amigxs con el epígrafe “te extrañarán” cuando intentas cerrar tu cuenta?

Pienso que con [Mastodon](https://mastodon.social) he creado un software que constituye *una alternativa realmente viable a Twitter*. Un servidor federado de microblogging que continúa el trabajo de GNU social, pero a diferencia de GNU social es capaz de atraer a personas sin un historial activo de interés en él. Para decirlo de otro modo, que es usable por personas sin conocimientos técnicos. No sé si el trabajo que estoy haciendo es suficiente para servir al futuro de la humanidad, pero creo que al menos es un buen paso, fuerte, en la dirección correcta.

Eugen Rochko,<br />
Desarrollador de [Mastodon](https://github.com/tootsuite/mastodon), administrador de [mastodon.social](https://mastodon.social), la instancia ejemplar.
