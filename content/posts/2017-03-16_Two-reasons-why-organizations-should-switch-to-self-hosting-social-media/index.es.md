---
title: "Dos razones por las que las organizaciones deberían cambiarse a redes sociales autoalojadas"
date: 2017-03-16
draft: false
author: gargron
categories:
- Op-Ed
tags:
- mastodon
- facebook
---

Mi nombre es Eugen Rochko y soy el creador de Mastodon, un servidor federado para redes sociales gratuito y de código abierto. La instancia principal mastodon.social cuenta con más de 23.000 usuarios y crece rápidamente. [Puedes chequearla aquí](https://mastodon.social).

Para tu organización social, alojar una instancia de Mastodon es esencialmente **una campaña de presencia que se autoperpetúa**. Cuando personas de otras instancias hablan o siguen a tus usuarixs, ven tu nombre todo el tiempo, dado que éste forma parte de sus nombres de usuarix. Es como esas pegatinas en los carros, solo que no necesitas pagar por ellas y no molestas a nadie, porque estás proporcionando un servicio.

{{< figure src="1*LM_XJwv6YpeZxBFMJVty_A.jpeg" caption="Hablando a un usuario de otra instancia" >}}

Twitter puso una barrera de pago a su API, con lo cual asfixió al ecosistema completo de la aplicación. Twitter censura publicaciones y cuentas en nombre de gobiernos como los de Estados Unidos y Turquía, y altera sus algoritmos de entrega de contenido de maneras que nadie puede conocer ni controlar. ¿Recuerdas cuando Facebook cambió los algoritmos de su hilo de noticias y de la noche a la mañana las publicaciones de cada página se volvieron virtualmente invisibles y lxs “fans” inútiles? A menos que le pagaras a Facebook, desde luego. **Estar a cargo de tu propio megáfono** en lugar de confiar en que una corporación te permita utilizar el suyo es cada vez más importante.

Por ejemplo, [aquí está la historia de una celebridad de los deportes electrónicos cuyo trabajo se volvió imposible porque dependía al 100% de Microsoft, que decidió bajarle la tecla](https://www.linkedin.com/pulse/big-microsoft-scam-sebastian-l%C3%A4ger) (en inglés).

Una instancia propia en Mastodon significa que **tu organización decide qué contenido alojar**. Recuperas el control que las plataformas comerciales monopolizaron. Y sin sacrificar la visibilidad: normalmente autoalojar un sitio web, foro o blog implica tener que atraer a todo el mundo desde otros sitios, pero la naturaleza federativa de Mastodon evita que las personas deban abandonar sus instancias para seguir la tuya.

* [Listado de instancias conocidas](https://github.com/tootsuite/mastodon/blob/master/docs/Using-Mastodon/List-of-Mastodon-instances.md)
* [Cómo instalar Mastodon en Ubuntu 16.04](https://github.com/tootsuite/mastodon/blob/master/docs/Running-Mastodon/Production-guide.md)
* [Instrucciones para ajustar el rendimiento](https://github.com/tootsuite/mastodon/blob/master/docs/Running-Mastodon/Tuning.md)
