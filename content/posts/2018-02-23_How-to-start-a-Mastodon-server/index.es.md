---
title: "Cómo iniciar un servidor de Mastodon"
subtitle: "Las partes no técnicas"
date: 2018-02-23
draft: false
author: gargron
resources:
- name: hero
  src: 1_oDIZtC5fQUG-COVspBloIw.jpeg
categories:
- Guides
tags:
- mastodon
---

De modo que quieres ser parte de la red Mastodon y disponer realmente de tu información sin que nadie más intervenga. Deseas nutrir una pequeña comunidad tal vez para dedicarse a un tema de interés específico, o quizás para tu familia o tu círculo de amistades más cercanas. Revisaste la [documentación](https://docs.joinmastodon.org) e instalaste el software; acaso elegiste una de las [opciones de alojamiento disponibles](https://masto.host) para evitar todo el sinsentido técnico de una vez. ¿Qué sigue a continuación?

Harás cosas dentro de tres categorías: personalización, federación y promoción. Si tu comunidad va a ser privada puedes saltar esa última parte.

### Personalización

Después de entrar (¡con privilegios de administrador en tu cuenta!), navega hasta Administración -> Ajustes del sitio *(Administration -> Site Settings)*. En la parte superior de la página se encuentra la información más importante sobre tu servidor. Puedes dejar el título como Mastodon, pero sin duda deberías especificar:

* Una dirección de correo electrónico para que la gente sepa a quién contactar si todo lo demás falla;
* El nombre de usuario de una persona de contacto (generalmente es tu propio nombre de usuario) para que la gente sepa a quién pertenece el servidor;
* Una breve descripción de un párrafo sobre el tema de tu servidor o lo que lo hace diferente de los otros.

También puedes subir una imagen de (preferentemente) 1200x630px para usar como ícono cuando tu servidor es enlazado desde otros sitios, como Slack, Discord o Twitter. JoinMastodon.org también muestra esa imagen cuando lista tu servidor (más sobre esto luego).

La siguiente y última parte crucial es añadir un código de conducta. Esto no es necesario para servidores privados, pero si esperas que se registren extrañxs, lo necesitas. Un código de conducta especifica qué tipo de comunidad y qué clase de contenido quieres alojar. Si no sabes por dónde empezar, muchos servidores han copiado el [código de conducta de mastodon.social](https://gist.github.com/Gargron/c2925b9ad7f5e87bf40c57a48de50474) (en inglés), elaborado en forma colectiva por su comunidad.


### Federación

*No deberías* abrir tu propio servidor si eres completamente nuevx en Mastodon, a menos que ya cuentes con una comunidad propia que traerás contigo. En un sistema descentralizado como Mastodon, el contenido viaja a través de una red de conexiones personales, de modo que si no tienes ninguna conexión, no tienes ningún contenido. Deberías empezar por abrir una cuenta en un servidor de Mastodon relativamente activo y hallar otras personas como tú.

*Recién entonces* podrás arrastrar esas conexiones contigo hacia tu propio servidor. Eso puede resultar bastante sencillo si en el otro servidor te diriges a Ajustes -> Exportar Datos *(Settings -> Data Export)* y descargas tu lista de seguidos como un archivo CSV, y luego en tu propio servidor te diriges a Ajustes -> Importar *(Settings -> Import)* y subes el archivo. De acuerdo a mi experiencia, deberías seguir al menos a 40 o 50 personas activas de otros servidores antes de lanzar el tuyo. Esto asegura un flujo constante de nuevo contenido (en tu cronología local solo son las personas a quienes sigues, pero en la cronología federada son ellxs y la gente que comparte e interactúa con ellxs).

Podría ser una percepción sesgada, pero considero que seguir admins de otros servidores suele ser un acierto. Suelen compartir abundante contenido de sus usuarixs, y así podrás tener una perspectiva completa de sus comunidades. Es posible que también quieras hacer lo mismo cuando tengas tus propixs usuarixs.

Cuando nuevas personas se unan a tu servidor, tendrán algo para ver y será más probable que se queden.

### Promoción

[JoinMastodon.org](https://joinmastodon.org) está diseñado para hacer parte de este trabajo por ti. Extrae la información de instances.social, un directorio independiente de servidores Mastodon. Una vez configurado tu correo de contacto en los Ajustes del Sitio *(Site Settings)*, deberías registrarte en [instances.social](https://instances.social/admin) e indicar en qué idiomas y qué categorías quieres aparecer listadx. En tanto y en cuanto tengas abiertas las registraciones y al menos un usuarix activx, deberías aparecer en JoinMastodon.org (en cualquier caso, no puedo garantizar esto: la prioridad de JoinMastodon.org es recibir a lxs nuevxs usuarixs lo más fácilmente posible, sin promocionar necesariamente a todxs y cada unx de lxs admins). De todos modos, aparecerás en instances.social, y eso también es importante.

Más allá de esto… construir comunidades es como hacer magia, y no hay fórmulas para el éxito. Difúndelo entre tu grupo de amistades. Cuando veas personas en otras redes sociales manifestar interés en alternativas a esas plataformas, sugiéreles tu instancia. ¡Buena suerte! 😋
