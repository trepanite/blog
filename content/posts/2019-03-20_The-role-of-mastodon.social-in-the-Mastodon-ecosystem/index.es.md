---
title: "El rol de mastodon.social en el ecosistema de Mastodon"
date: 2019-03-20
draft: false
author: gargron
categories:
- Op-Ed
tags:
- mastodon
- decentralization
---

¿Te imaginas que Facebook cerrara los registros diciendo “Disculpa, ya tenemos demasiados usuarios, ¿por qué mejor no vas a registrarte a Twitter?”. Y sin embargo, una situación parecida se presenta en Mastodon cada cierto tiempo, con relación al servidor mastodon.social.

Como sabes, Mastodon es descentralizado. Esto significa que no hay un servidor “central”. Si mastodon.social desapareciera de la faz de la tierra, no afectaría en absoluto a la red Mastodon. Pero es uno de los servidores más grandes, lo cual significa que si observas la base de usuarios registrados, verás que de hecho está centralizado. Después de todo, 300.000 no es una porción menor de 2.000.000.

Ninguna otra red social tiene este problema, o más bien, no lo considerarían un problema. Pero hay quien opina que el proyecto Mastodon debería promover activamente la descentralización en términos de distribución de usuarixs, lo cual presenta un desafío particular. Francamente, el único antecedente que se me ocurre, oscuro como es, y a una escala mucho menor, es la distribución de lxs jugadorxs de World of Warcraft en diferentes reinos que hizo Blizzard.

El desafío está en lo siguiente: dado que la mayoría de las otras redes sociales son centralizadas, en el imaginario de la gente “registrarse en Mastodon” equivale a “registrarse en mastodon.social”. Explicar la diferencia, la importancia de la diferencia, y lograr que alguien elija a conciencia un servidor de entre una variedad increíblemente amplia, dentro del estrecho rango de atención de una persona medianamente curiosa, no es sencillo.

He intentado resolver el asunto casi desde que Mastodon existe. Hay muchos beneficios en no tener a todxs lxs usuarixs en el mismo servidor, como describí [en un artículo diferente]({{< ref "posts/2018-12-30_Why-does-decentralization-matter/index.es.md" >}}).

El problema tiene dos dimensiones. Una, que cuando la persona llega directamente a la dirección mastodon.social en lugar de a joinmastodon.org no hay modo de asegurar que se registre en otra instancia: solo puedes asegurar que no se registre en ésta. Puedes cerrar las inscripciones, poner un mensaje enlazando a joinmastodon.org. ¡Disculpa, estamos llenos!

La otra dimensión es cuando la gente llega a joinmastodon.org, de acuerdo a lo esperado. Hay una larga lista, filtrable, de servidores preparados para aceptar nuevxs miembrxs, que se supone que la gente recorra para encontrar el que les agrade. Aquí simplemente puedes ocultar mastodon.social de la lista y que no sea una opción para que la gente elija. ¡Problema resuelto!

Pero...

Estas soluciones resuelven un problema, solo para crear otro.

Cuando cierras las inscripciones y pones un enlace para ir a otra parte, la realidad de la situación es que habrá cierta cantidad de gente que simplemente lo abandonará, perdiendo el interés en este punto. ¿Y si no es así, si efectivamente siguen el enlace a joinmastodon.org? Elegir es difícil. La mayoría de los servidores de Mastodon están centrados en identidades o intereses específicos. ¿Estás estudiando? scholar.social. ¿Eres fotógrafo? photog.social. ¿Videojuegos? elekk.xyz. Pero, ¿qué tal si no sientes que pertenezcas a ninguna categoría particular como esas? Twitter no te obligó a decidir tus intereses de antemano. Los servidores de temas generales parecen ser una rareza. E incluso de los que hay por ahí, no todos tienen el beneficio de llevar “mastodon” en el nombre de dominio.

En verdad se siente como que el crecimiento del fediverso se desacelera cuando mastodon.social no está disponible.

Es una desición difícil. He cerrado y reabierto las inscripciones en mastodon.social muchas veces en el transcurso de su historia. Definitivamente hay un peligro en la centralización de facto, y me preocupa por ejemplo, la hegemonía de Gmail en el ecosistema de correos electrónicos. Pero también pienso que el crecimiento es clave para la red, porque de otra manera no será capaz de competir contra las alternativas centralizadas. Lxs músicxs no se preguntarán si cada uno de los 4000 servidores tiene un número equivalente de usuarixs; elegirán la red donde vean la mejor perspectiva para llegar a lxs fans o conectar con otrxs músicxs.

Cabe mencionar que muchas personas que ahora administran servidores grandes y activos de Mastodon empezaron con una cuenta en mastodon.social. Es la opción simple para registrarse sin saber nada más, y es mucho más fácil enseñar sobre la descentralización a alguien que ya está en Mastodon que, digamos, enseñar a alguien que perdió el interés en Mastodon porque fue rechazado y volvió a Twitter.

Hoy [reabriré las inscripciones en mastodon.social](https://mastodon.social/about) luego de casi tres meses. No sé si siempre podré mantenerlas abiertas, o si aparecerá alguien con maneras más efectivas de incorporar nuevxs usuarixs, pero aquí dejo una explicación para el pasado y el futuro sobre un tema bastante controvertido.
