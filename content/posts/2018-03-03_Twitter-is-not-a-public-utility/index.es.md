---
title: "Twitter no es un bien público"
date: 2018-03-03
draft: false
author: gargron
resources:
- name: hero
  src: 1_thnUPTnBOJX30rBroH3Jzw.jpeg
categories:
- Op-Ed
tags:
- twitter
- surveillance
---

¿No resulta algo extraño que el mundo entero deba [esperar a que el CEO de Twitter se decida a determinar qué constituye un nivel de discurso saludable](https://twitter.com/jack/status/969234275420655616)? No me refiero a que sea demasiado poco, demasiado tarde. Mi problema está más bien en que “la mensajería y la conversación global, instantánea y pública” dependan completamente de los caprichos de una única empresa privada. Puede ser que ahora, por una vez, deseen moverse en la dirección correcta, pero, ¿quién puede decir cuáles serán sus cambios de opinión en el futuro? ¿A quién responde Twitter, fuera de su junta directiva?

Todavía me resulta difícil de creer cuando Jack Dorsey dice que los actos de Twitter no están motivados por el intento de aumentar la cotización de sus acciones. Twitter tiene que mantener felices a sus accionistas para sobrevivir, y *casualmente ocurre* que los bots y los intercambios negativos en su plataforma empujan sus métricas de interacción hacia arriba. Cada vez que alguien cita un tuit para subrayar algo tóxico, logra que sus seguidores interactúen con eso y el ciclo continúe. Es sabido que [el escándalo se propaga más rápidamente](https://www.youtube.com/watch?v=rE3j_RHkqJc) que el contenido motivador y positivo, por lo que, desde un punto de vista financiero, no tendría sentido que Twitter se deshaga de las fuentes de escándalo (y su registro de seguimiento es un testimonio de esto).

En mi opinión, “la mensajería y la conversación global, instantánea y pública” debería ser, de hecho, *global*. Distribuida entre organizaciones independientes y actores capaces de ejercer el autogobierno. Una utilidad pública, sin incentivos para explotar conversaciones y obtener ganancias. Una utilidad pública, que sobreviva a la limitada autonomía de todas estas redes sociales descartables. Esto es lo que me motivó al crear [Mastodon](https://joinmastodon.org).

Además, Twitter continúa encarando el asunto por el lado equivocado. Como la moda en Silicon Valley es usar aprendizaje automatizado para todo, Twitter va a hacer análisis de sentimiento y qué no cuando en realidad… solo necesitan moderación humana. Alguien con quien lxs usuarixs puedan hablar, alguien capaz de comprender el contexto. Absolutamente fuera de escala para Twitter, donde millones de personas se hacinan bajo un solo reglamento, pero natural para Mastodon, donde los servidores son pequeños y tienen sus propixs administradorxs.

Twitter no es una utilidad pública. Esto nunca va a cambiar. Y cada tuit quejándose al respecto simplemente hace que su reporte trimestral luzca mejor.

*¡Para empezar con Mastodon, visita [JoinMastodon.org](https://joinmastodon.org/) y escoge un lugar al que llamar hogar! ¡Usa los menús desplegables para afinar tu búsqueda por intereses e idioma, y encuentra una comunidad que sientas como propia! ¡No dejes que el fediverso se pierda lo que tienes para decir!*
