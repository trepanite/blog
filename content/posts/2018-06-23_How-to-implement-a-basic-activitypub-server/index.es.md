---
title: "Implementación básica de un servidor ActivityPub"
date: 2018-06-23
draft: false
author: gargron
categories:
- Guides
tags:
- mastodon
- activitypub
---

Hoy veremos cómo conectar los protocolos que potencian Mastodon para entrar de la manera más simple posible a la red federada. Usaremos archivos estáticos, herramientas comunes de la línea de comandos y algo de código en Ruby, aunque la funcionalidad debería resultar fácilmente adaptable a otros lenguajes de programación.

Primero, ¿cuál es la meta final de este ejercicio? Queremos enviar a un usuario de Mastodon un mensaje desde nuestro servidor propio, no-Mastodon.

¿Qué ingredientes necesitamos? El mensaje estará formateado con ActivityPub, y para ello debe ser atribuido a un actor ActivityPub. El actor debe ser localizable mediante Webfinger, y la entrega debe estar firmada criptográficamente por el actor.

### El actor

El actor es un documento JSON-LD públicamente accesible que responde a la pregunta “quién”. El JSON-LD en sí mismo es una bestia bastante complicada, pero por suerte podemos tratarlo como un simple JSON con un atributo `@context` para nuestros fines. Así es como el documento de un actor se vería:

```json
{
	"@context": [
		"https://www.w3.org/ns/activitystreams",
		"https://w3id.org/security/v1"
	],

	"id": "https://my-example.com/actor",
	"type": "Person",
	"preferredUsername": "alice",
	"inbox": "https://my-example.com/inbox",

	"publicKey": {
		"id": "https://my-example.com/actor#main-key",
		"owner": "https://my-example.com/actor",
		"publicKeyPem": "-----BEGIN PUBLIC KEY-----...-----END PUBLIC KEY-----"
	}
}
```
La `id` (identificación) debe ser la URL del documento (es una autorreferencia), y todas las URLs deben usar HTTPS. Necesitas incluir una `inbox` (bandeja de entrada) incluso aunque recibir mensajes de respuesta no esté en tus planes, porque Mastodon no reconoce a los actores sin bandeja de entrada como compatibles.

La parte más complicada de este documento es la `publicKey` (clave pública), dado que involucra criptografía. Aquí la `id` remitirá al actor, con un fragmento (la parte después de `#`) para identificarlo (esto porque no vamos a alojar la clave en un documento aparte, aunque podríamos hacerlo). El `owner` (propietario) debe ser la `id` del actor. Ahora la parte difícil: debes generar un par de llaves RSA.

Puedes hacer esto usando OpenSSL:

    openssl genrsa -out private.pem 2048
    openssl rsa -in private.pem -outform PEM -pubout -out public.pem

El contenido del archivo `public.pem` es lo que iría dentro de la propiedad `publicKeyPem`. Sin embargo, JSON no acepta cadenas con saltos de línea, así que primero debes reemplazarlos por `\n`.

### Webfinger

¿Qué es Webfinger? Es lo que nos permite preguntarle a un sitio: “¿tienes un usuario con este nombre de usuario?” y recibir enlaces fuente como respuesta. En nuestro caso implementar esto es facilísimo, ya que no estamos interfiriendo con ninguna base de datos y podemos *hardcodear* todo lo que queramos.

El resultado de Webfinger siempre está en `/.well-known/webfinger`, y recibe solicitudes tales como `/.well-known/webfinger?resource=acct:bob@my-example.com`. Bueno, en nuestro caso podemos hacer trampa y simplemente crear un archivo estático:

```json
{
	"subject": "acct:alice@my-example.com",

	"links": [
		{
			"rel": "self",
			"type": "application/activity+json",
			"href": "https://my-example.com/actor"
		}
	]
}
```

Aquí la propiedad `subject` (sujeto) es tu nombre de usuarix (el mismo que antes en `preferredUsername`) y el dominio donde estés alojadx. De esta forma se almacenará a tu actor en otros servidores de Mastodon y la gente podrá mencionarte en sus toots. Webfinger solo necesita un enlace para la respuesta, y es el enlace al documento del actor.

Una vez que esto esté subido a tu servidor web y accesible bajo tu propio dominio con un certificado SSL válido, ya deberías poder encontrar a tu actor desde otro Mastodon ingresando `alice@my-example.com` en la barra de búsqueda. Aunque se verá bastante desolado.

### El mensaje

En la práctica, los mensajes de ActivityPub tienen dos partes: el mensaje en sí (el objeto) y un wrapper (envoltorio) que comunica lo que sucede con el mensaje (la actividad). En nuestro caso, será una actividad `Create` (Crear). Digamos “Hola mundo” en respuesta a mi toot sobre la creación de esta entrada:

{{< mastodon "https://mastodon.social/@Gargron/100254678717223630" >}}

Así es como se vería el documento:

```json
{
	"@context": "https://www.w3.org/ns/activitystreams",

	"id": "https://my-example.com/create-hello-world",
	"type": "Create",
	"actor": "https://my-example.com/actor",

	"object": {
		"id": "https://my-example.com/hello-world",
		"type": "Note",
		"published": "2018-06-23T17:17:11Z",
		"attributedTo": "https://my-example.com/actor",
		"inReplyTo": "https://mastodon.social/@Gargron/100254678717223630",
		"content": "<p>Hello world</p>",
		"to": "https://www.w3.org/ns/activitystreams#Public"
	}
}
```

Con la propiedad `inReplyTo` (enRespuestaA) estamos encadenando nuestro mensaje a un hilo. La propiedad `content` (contenido) puede incluir HTML, aunque por supuesto será sanitizada por los servidores que la reciban de acuerdo a sus necesidades (diversas implementaciones pueden utilizar diversos conjuntos de marcado). Mastodon solo conservará las etiquetas: `p`, `br`, `a` y `span`. Con la propiedad `to` definimos quién podrá ver nuestro mensaje; en este caso es un valor especial que significa “todo el mundo”.

Para nuestro objetivo no necesitamos realmente alojar este documento públicamente, aunque lo ideal es que tanto la actividad como el objeto estén disponibles por separado, cada cual con su respectiva `id`. Solo vamos a guardarlo como `create-hello-world.json` (crear-hola-mundo.json), porque más adelante lo necesitaremos.

Entonces, la siguiente pregunta es: ¿cómo enviamos este documento, a dónde, y cómo podrá Mastodon considerarlo confiable?

### Firmas HTTP

Para enviar nuestro mensaje usaremos un comando POST hacia la bandeja de entrada de la persona a la que le respondemos (en este caso, yo). Esa bandeja es `https://mastodon.social/inbox`. Pero un simple POST no será suficiente, porque, ¿cómo va a saber alguien que el mensaje proviene de la @alice@my-example.com real y no literalmente de cualquier otra persona? Para eso necesitamos una firma HTTP. Es un encabezado HTTP firmado por el par de llaves RSA que generamos antes y asociado a nuestro actor.

Las firmas HTTP son una de esas cosas mucho más fáciles de hacer mediante código que manualmente. Nuestra firma se ve así:

    Signature: keyId="https://my-example.com/actor#main-key",headers="(request-target) host date",signature="..."

La `keyId` remite a la llave pública de nuestro actor, el `header` (encabezado) enlista los encabezados usados para construir la firma y luego, finalmente, la cadena de la `signature` (firma) misma. El orden de los encabezados debe ser igual en texto plano y dentro de la cadena que se va a firmar, y los nombres de encabezados deben ser siempre en minúsculas. El de `(request-target)` (solicitar-destino) es un encabezado especial, falso, que sirve para fijar el método HTTP y la ruta de destino.

Más o menos así se verá la cadena que se va a firmar:

    (request-target): post /inbox
    host: mastodon.social
    date: Sun, 06 Nov 1994 08:49:37 GMT

Ten en cuenta que hay una ventana de tiempo de solo ±30 segundos durante los que la firma se considera válida, lo cual es una razón de peso para que sea bastante difícil hacerlo manualmente. De todas maneras, asumiendo que tenemos una fecha válida, ahora solo debemos obtener una cadena firmada. Juntémoslo todo:

```ruby
require 'http'
require 'openssl'

document      = File.read('create-hello-world.json')
date          = Time.now.utc.httpdate
keypair       = OpenSSL::PKey::RSA.new(File.read('private.pem'))
signed_string = "(request-target): post /inbox\nhost: mastodon.social\ndate: #{date}"
signature     = Base64.strict_encode64(keypair.sign(OpenSSL::Digest::SHA256.new, signed_string))
header        = 'keyId="https://my-example.com/actor",headers="(request-target) host date",signature="' + signature + '"'

HTTP.headers({ 'Host': 'mastodon.social', 'Date': date, 'Signature': header })
    .post('https://mastodon.social/inbox', body: document)
```

Vamos a guardarlo como `deliver.rb`. Aquí uso el gem HTTP.rb, que deberás tener instalado (`gem install http`). Finalmente, ejecuta el archivo con `ruby deliver.rb`, ¡y tu mensaje debería aparecer como una respuesta en mi toot!

### Conclusión

Vimos cómo crear un actor ActivityPub localizable y y cómo enviar respuestas a otras personas. Pero queda mucho por cubrir: cómo seguir y ser seguido (requiere una bandeja de entrada en funcionamiento), cómo hacer un perfil más atractivo, cómo soportar el reenvío de documentos con firmas LD y más. ¡Si hay demanda, continuaré escribiendo tutoriales en profundidad!

Lee más aquí (en inglés):

* [ActivityPub](https://www.w3.org/TR/activitypub/)
* [Webfinger](https://tools.ietf.org/html/rfc7033)
* [HTTP Signatures](https://tools.ietf.org/html/draft-cavage-http-signatures-10)
