---
title: "Aprendiendo de los errores de Twitter"
subtitle: "Herramientas de privacidad y moderación contra el abuso en Mastodon"
date: 2017-03-03
draft: false
author: gargron
resources:
- name: hero
src: 1_41Gx5rCc2E4g1f6xaeHJGg.jpeg
categories:
- New Features
tags:
- mastodon
- twitter
---

*Mi nombre es Eugen Rochko y soy el creador de Mastodon, un servidor federado para redes sociales gratuito y de código abierto. La instancia principal mastodon.social cuenta con más de 22.000 usuarios y crece rápidamente. [Puedes chequearla aquí](https://mastodon.social).*

Muy temprano en el desarrollo de Mastodon, decidí que la centralización y los cambios arbitrarios del algoritmo no eran el único de los problemas de Twitter. Las herramientas de privacidad para enfrentar el acoso y los abusos siempre fueron débiles en Twitter. Me dirigí a personas afectadas por esto para recolectar ideas. Aquí está lo que he reunido:

Cuando bloqueas a una persona no quieres volverla a ver, nunca. Esto significa que si alguien a quien sigues comparte las publicaciones de esa persona, no las quieres ver. Si alguien habla de esa persona, no lo quieres ver. Si alguien responde a las publicaciones de esa persona mencionándote, no quieres verlo. Así es como debería funcionar, y así funciona en Mastodon.

Pero es posible que no quieras ir tan lejos. Tal vez solo quieres dejar de ver las publicaciones de una persona, pero no bloquearla completamente. Silenciar una cuenta para quitarla de tu hilo es una opción intermedia.

Por tu parte, puedes ocultar el texto de cada publicación individual bajo una advertencia de contenido (tú decides si es por spoiler o por sensibilidad) y, al compartir imágenes que no te gustaría que te vieran viendo en público, puedes marcar individualmente que la publicación contiene material sensible.

En algunas ocasiones quieres transmitir abiertamente a la red y en otras solo quieres dirigirte a las personas que conoces. Para este fin una opción es activar el requisito de que todxs tus nuevxs seguidorxs deban obtener tu aprobación antes de que se les permita seguirte. Pero también puedes determinar individualmente la visibilidad de tus publicaciones: o públicas o solo visibles para tus seguidorxs y las personas mencionadas. La existencia de cronologías públicas (hilos que incluyen publicaciones “de todo el mundo”) requiere un terreno intermedio: en este caso podrás indicar que tus publicaciones no aparezcan listadas en las cronologías públicas, pero de todos modos sean abiertas a la red.

Si te encuentras con contenido inapropiado, hay una opción para denunciar la cuenta que permite señalar las publicaciones ofensivas y, opcionalmente, especificar un mensaje.

En el caso de que sepas exactamente con quiénes quieres hablar y con quiénes no, tienes la posibilidad de silenciar las notificaciones de personas que no te siguen. De este modo no volverás a ver ningún contenido de desconocidxs. Y si además activas la opción de silenciar las notificaciones de personas a quienes tú no sigues, quedarás solo con lxs “mutuxs” (solo lxs que te siguen y lxs sigues, como si fuera un grupo privado).

La naturaleza federada de la red repercute en nuestro comportamiento. Diferentes instancias, pertenecientes a diferentes entidades, tendrán diferentes reglas y políticas de moderación. Esto devuelve a la gente el poder de formar comunidades pequeñas, independientes y sin embargo integradas. Como usuarix, tienes la posibilidad de elegir la instancia que proponga las reglas y políticas con las te encuentres más a gusto (o formar la tuya, si se te dan bien las cuestiones técnicas) sin que esto te impida participar de las otras.

Por otra parte, las comunidades pequeñas, de lazos estrechos, son menos proclives a propiciar el comportamiento tóxico. El hecho de que el trabajo de moderación de toda la red se encuentre distribuido entre lxs numerosxs administradorxs de comunidades independientes pero compatibles*, lo hace mucho más fiable que en una empresa de organización centralizada y vertical, con miles de millones de usuarixs y un pequeño equipo de moderación.

*(Cabe aclarar que la moderación de la red no es global. Quien administra una instancia no puede afectar las cuentas de usuarixs de otra instancia. Lxs administradorxs son responsables del contenido que surge de sus instancias, y cuentan con varias herramientas para controlarlo. Esto permite que sitios con reglas diferentes puedan coexistir.)*

Por supuesto, también aparecerán comunidades con el único propósito de difundir actitudes tóxicas. En tal caso, lxs administradorxs de instancias saludables pueden poner en lista negra a una instancia específica completa. A diferencia de la facilidad que supone crear una nueva cuenta de spam, bulling o trolling en una red centralizada, desplegar toda una nueva instancia en Mastodon requiere un esfuerzo significativamente mayor —volver a adquirir alojamiento y un nuevo nombre de dominio, invertir tiempo en la instalación y configuración—, por lo que evadir las listas negras es mucho más difícil.

Con todo esto, [Mastodon](https://mastodon.social) apunta ser un sitio más humano y más seguro.

*Si quieres revisar estas configuraciones por ti mismx, ve a [mastodon.social](https://mastodon.social).*
