---
title: "¿Por qué es importante la descentralización?"
description: "Razones por las que deberías interesarte en la estructura y funcionamiento de tu red social"
date: 2018-12-30
draft: false
author: gargron
resources:
- name: hero
  src: decentralization.png
categories:
- Op-Ed
tags:
- mastodon
- facebook
- twitter
- decentralization
---

Hace dos años que escribo sobre Mastodon, y se me ocurrió que nunca expuse en un texto claro y conciso las razones por las que cualquiera debería interesarse en la descentralización. Desde luego que lo expliqué en varias entrevistas, y algunos de los argumentos aparecen aquí y allá entre el material promocional, pero este artículo debería responder a la pregunta de una vez por todas.

> **descentralización**, sust.: dispersión o distribución de funciones y poderes; delegación del poder de una autoridad central a autoridades regionales y locales.
>
> **fediverso**, sust.: La red social descentralizada conformada por Mastodon, Pleroma, Misskey y otras a través del estándar ActivityPub.

Entonces, ¿por qué es tan importante? La descentralización supera el modelo de negocios de las redes sociales mediante una reducción extraordinaria de los costos operativos porque evita que una única entidad deba cargar con todos los costos. Así, ningún servidor descentralizado necesita salir de su zona de confort ni sobreexigir su capacidad financiera. Como la inversión inicial es cercana a cero, el operador de una instancia de Mastodon no tiene que acudir al capital especulativo, que lo presionaría para que utilice esquemas de monetización a gran escala. Existe una razón por la que los ejecutivos de Facebook desmantelaron el modelo de negocios de Whatsapp “1 dólar por año” después de adquirirla: era sostenible y relativamente justo, pero no proporcionaba el retorno de inversión impredecible y potencialmente infinito que hace subir el precio de las acciones. La publicidad sí.

Si eres Facebook, bien por ti. Pero si eres usuarix de Facebook, bueno... los intereses de la empresa y los de lxs usuarixs son opuestos. De ahí el viejo adagio que dice que si no lo pagas, eres el producto. La situación se deja traslucir en patrones oscuros tales como hacer que por defecto los hilos sean no-cronológicos (ya que de este modo al usuarix se le dificulta saber si ya ha visto toda la página, lo cual aumenta los desplazamientos y actualizaciones, lo cual aumenta las impresiones de anuncios), enviar correos sobre notificaciones sin leer que en realidad no existen, o rastrear tu comportamiento de navegación por todo internet para saber quién eres...

La descentralización es como la biodiversidad en el mundo digital: es el rasgo distintivo de un ecosistema saludable. Una red descentralizada, como el fediverso, permite que distintas interfaces, distintos programas o distintas formas de gobierno coexistan y cooperen. Y si ocurre algún desastre, algunas estarán mejor adaptadas que otras y sobrevivirán, cosa que en una “monocultura” no sucedería. No se necesita pensar mucho para encontrar ejemplos recientes: sin ir más lejos, las leyes FOSTA/SESTA sancionadas en EEUU fueron terribles para lxs trabajadorxs sexuales, y afectan a todas y cada una de las redes sociales dominantes, porque todas están basadas en EEUU. En Alemania, por ejemplo, el trabajo sexual es legal: ¿por qué lxs trabajadorxs sexuales de Alemania no iban a poder utilizar las redes sociales?

Una red descentralizada también es más resistente a la censura (y me refiero a la verdadera censura, no la de “¡buuu, no me permiten publicar esvásticas!”). Algunos dirán que una gran corporación puede resistir mejor las presiones de los gobiernos. Pero en la práctica, las plataformas comerciales solo luchan contra las presiones gubernamentales en los mercados donde aún no logran imponer sus monopolios. Toma por ejemplo a Google y su deslucida oposición a la censura en China, o los frecuentes bloqueos de Twitter a activistas y periodistas en todo el mundo. La fortaleza de una red descentralizada reside en el poder del gran número: algunos servidores podrán ser bloqueados, algunos se someterán, pero no todos caerán. Y crear nuevos servidores es fácil.

Y último, pero no menos importante: la descentralización contrarresta la asimetría de poder. Cualquier plataforma de medios sociales centralizada tiene una estructura jerárquica donde las reglas y su aplicación, así como el desarrollo y la dirección de la plataforma son decididos por el CEO, dejando a lxs usuarixs prácticamente sin recursos para expresar su acuerdo o desacuerdo. Abandonar la plataforma donde están todas tus amistades, tus contactos y tu audiencia no es una solución. Una red descentralizada renuncia deliberadamente al control de la plataforma por parte del propietarix, sencillamente porque no tiene unx. Por ejemplo, como desarrollador de Mastodon, mi única influencia es a nivel de asesoría: puedo desarrollar nuevas características y publicar nuevas versiones, pero no puedo forzar a nadie a instalarlas si no quieren; no tengo control sobre ningún servidor de Mastodon excepto el mío; no tengo más poder sobre Mastodon que el que tengo sobre cualquier otro sitio en internet. Esto significa que la red no está sujeta a mis caprichos: se puede adaptar a los cambios más rápidamente que yo, y puede resultar útil para casos de uso que jamás se me hubieran ocurrido.

Cualquier red social alternativa que no impulse la descentralización acabará estancándose frente a estas dificultades. Y si no perece, como las que lo intentaron y fracasaron antes, simplemente se convertirá en aquella que pretendía reemplazar.

**Cavando más hondo:**

- [How Mastodon uses decentralization](https://docs.joinmastodon.org/)
- [The nature of the self in the digital age](https://2018.ar.al/notes/the-nature-of-the-self-in-the-digital-age/)
