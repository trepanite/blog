---
title: "Por qué ActivityPub es el futuro"
date: 2018-06-27
draft: false
author: gargron
resources:
- name: hero
  src: ezgif-2-60f1b00403.gif
  params:
    resize: false
categories:
- Op-Ed
tags:
- activitypub
- peertube
- pixelfed
- misskey
- pleroma
- plume
---

Con frecuencia tuteamos cosas como que “Mastodon está basado en protocolos de red abiertos” como una de sus ventajas. Quisiera ampliar por qué exactamente esto es bueno.

A modo de repaso, Mastodon implementa el protocolo denominado ActivityPub, que permite a los servidores comunicarse entre sí. Esta es la base de la “federación” que también nos gusta mencionar a menudo. La federación es algo que ya conoces del correo electrónico, aunque no supieras su nombre: es el concepto de que un servidor brinde alojamiento a usuarixs que pueden contactar a lxs usuarixs alojados por otro servidor. Dicho protocolo pone sobre el papel exactamente cómo funciona esa comunicación entre servidores, utilizando un lenguaje que se puede aplicar a diversos fines. Y he aquí la sorpresa:

**La red social Mastodon en realidad no es Mastodon**. Es *más grande*. Es *cualquier software que implemente ActivityPub*. ¡Cada sistema puede variar enormemente en sus funciones y su apariencia! Pero el grafo social (así llamamos a la gente y sus conexiones) es siempre el mismo.

- Mastodon es el software construido en torno a publicaciones de texto de 500 caracteres.
- ¿Quieres una plataforma de videos? Ahí está [PeerTube](https://joinpeertube.org/en/home/).
- ¿Quieres algo centrado en fotos e imágenes? [PixelFed](https://pixelfed.org/) está aquí.
- ¿Quieres escribir entradas largas y complejas? [Plume](https://joinplu.me/) en desarrollo.

Eso por no mencionar una multitud de variaciones bajo los mismos conceptos. PeerTube y PixelFed no serán los únicos en sus categorías. Por ejemplo, [Misskey](https://join.misskey.page/en-US/) y [Pleroma](https://pleroma.social/) proponen casos de uso similares al de Mastodon, pero con decisiones diferentes en cuanto al lenguaje de programación, el diseño y la funcionalidad.

Estas plataformas son distintas y se enfocan en diferentes necesidades. Aún así, el fundamento es el mismo: gente suscribiéndose para recibir publicaciones de otra gente. Luego todas son compatibles. En Mastodon, Pleroma, Misskey, PixelFed y PeerTube lxs usuarixs a lxs que se pueden seguir y con quienes se puede interactuar son lxs mismxs.

**Y esta es la fortaleza de utilizar protocolos de red abiertos**. Cuando decides cambiarte a Mastodon, no solo estás apostando al éxito de un proyecto. Puedes contar con que, más allá de lo que suceda con Mastodon, la red continuará viva y floreciendo. Nacerá nuevo y mejor software dentro del ecosistema, pero nunca tendrás que arrastrar a todas tus amistades y seguidorxs a una nueva plataforma otra vez (ya estarán allí donde deben estar).

Si Twitter cierra, perderás tus seguidorxs. Si Facebook cierra, perderás tus amistades. Para ciertas plataformas la cuestión no es “si”, sino “cuándo”. Tras eventos como estos suele ocurrir una disgregación hacia una variedad de plataformas diferentes, donde inevitablemente pierdes algo de gente cuando debes elegir a cuál migrar. Esto ha sucedido antes. Pero no tiene que volver a pasar. Usa la red federada. [Únete a Mastodon](https://joinmastodon.org/).
