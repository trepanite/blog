---
title: "Encierren al Mastodonte"
subtitle: "Un vistazo a las herramientas de moderación contra el abuso y el acoso"
date: 2018-07-06
draft: false
author: gargron
resources:
- name: hero
  src: vlcsnap-2018-07-05-01h09m25s103.png
categories:
- Guides
tags:
- mastodon
- abuse
---

Un año atrás escribí sobre las [mejoras de Mastodon con respecto a Twitter para la protección contra el abuso y el acoso]({{< ref "posts/2017-03-03_Learning-from-Twitter-s-mistakes/index.es.md" >}}). El desarrollo en este área no se detuvo, y es hora de hacer una nueva comparación.

Primero un repaso de los fundamentos, que no cambiaron: Mastodon está descentralizado entre servidores o instancias que operan de forma independiente. Cada servidor cuenta con su administrador/a, y posiblemente un equipo de moderadorxs, así como su propio código de conducta. De allí que:

* Puedes estar en una comunidad que coincida con tus valores e ideales de moderación;
* La proporción entre “toda la gente” y “la gente capaz de responder a un reporte de abuso” es mucho, mucho mayor que en cualquier servicio centralizado;
* El equipo de moderadorxs de tu instancia, ya sea un administrador/a o varias personas, es mucho más cercano y accesible, por lo que puede abordar cualquier situación de agresión con seriedad.

Solo esto, que está incorporado al diseño básico del sistema, representa una ventaja enorme sobre otras plataformas. Y también ofrece ventajas sobre el modelo del otro extremo: en un diseño de estricto P2P (par-a-par) cada cual debería cuidar de sí mismx, sin recursos colectivos. Pero hay más.

### Decisiones en cuanto al diseño

Antes de continuar, debo aclarar que las decisiones de diseño están más relacionadas con los comportamientos que el sistema promueve y favorece que con una barrera infalible contra ciertos comportamientos, lo cual no es posible. Por ejemplo, **es deliberado que Mastodon no proporcione una búsqueda general arbitraria**. Si alguien quiere que su mensaje sea descubierto, puede utilizar una etiqueta, que se puede abrir, pero, ¿qué logra la búsqueda arbitraria? Que las personas y las marcas busquen su propio nombre para auto-insertarse en conversaciones a las que no fueron invitadas.

De todos modos sí puedes buscar entre los contenidos que publicaste, recibiste o marcaste. Así podrás hallar rápidamente ese mensaje que tenías en la punta de la lengua.

Otra característica, reclamada casi desde el primer momento y que sigo rechazando, es la de **citar mensajes**. Volviendo sobre mi aclaración sobre los comportamientos, desde luego que es imposible evitar que la gente comparta capturas de pantalla o enlaces a fuentes públicas, pero citar mensajes sería una opción *de uso inmediato*, lo cual facilitaría que la gente interactúe rápidamente con el contenido citado... y eso no suele acabar bien. Cuando la gente usa citas para responderle a otras personas, las conversaciones se convierten en duelos de poder. "¡Vean, mis seguidorxs, cómo aplasté a este estúpido!". Muy frecuentemente con el uso de una cita se convoca a lxs seguidorxs a unirse a la conversación. Luego quien consiga más participantes tendrá la mano ganadora y humillará en masa a la otra persona. En cambio, cuando utilizas la función de respuesta tus mensajes se transmiten únicamente a las personas que siguen a ambxs interlocutorxs. Esto significa que el número de seguidorxs de una persona no es un factor determinante en la conversación.

Twitter te obliga a elegir entre dos extremos: una cuenta privada o una cuenta pública. Si tienes una cuenta pública, todos tus tuits son visibles para todo el mundo y se pueden compartir hasta el infinito. Pero tal vez eso no siempre sea lo que quieres. En Mastodon **cada mensaje individual puede ser**:

- Totalmente público. Visible para tus seguidorxs, las cronologías públicas y cualquiera que visite tu perfil;
- Sin listar. Visible para tus seguidorxs y cualquiera que visite tu perfil pero no en las cronologías públicas;
- Privado. Solo visible para tus seguidorxs y las personas mencionadas;
- Directo. Solo visible para la o las personas mencionadas.

El modo sin listar es perfecto para mantener un bajo perfil y no atraer a extraños a que interactúen contigo, mientras que privado es muy útil cuando no quieres que tus publicaciones sean fácilmente compartidas y circulen de aquí para allá. Cada opción se puede configurar como predeterminada. Y, desde luego, además de eso **puedes “bloquear” tu cuenta**: así evitas que las personas te sigan sin darte la chance de aceptar o rechazar su solicitud primero.

Mastodon cuenta con la opción de listas para categorizar a las personas que sigues y hacer más legible tu hilo principal, esencialmente dividiéndolo en varios hilos, pero a diferencia de Twitter, **no puedes añadir a alguien a una lista a menos que ya lo estés siguiendo**, y las listas son personales, no públicas.

### Ocultando cosas

Mastodon proprociona una gran cantidad de opciones para ocultar el contenido no deseado. ¿Te molesta gente extraña? Puedes **bloquear las notificaciones de personas a las que no sigues**. ¿Quieres que alguien deje de ver tus publicaciones y olvidarte de que existe? La opción de **bloquear** oculta las notificaciones de esa persona, los mensajes que la mencionan y los mensajes de esa persona que otros comparten. ¿Quieres lo mismo pero que la persona bloqueada no lo sepa? Esa es la opción de **silenciar**.

¿Te cansaste de recibir respuestas a una de tus publicaciones? Puedes **silenciar la conversación** y olvidar el asunto. Y si notas que estás silenciando a demasiada gente de la misma instancia, y no se detienen, puedes **ocultar todo lo de un dominio específico por completo**. Eso ocultará todas sus publicaciones y eliminará a lxs seguidorxs que tengas de esa instancia para que tampoco reciban tus publicaciones.

En las próximas semanas, a partir de la version 2.4.3, podrás silenciar un tema específico en forma temporal o permanente con **filtros de texto**. Estos filtros buscan palabras clave o frases en las publicaciones y las ocultan, tanto en general como solo en contextos específicos.

Por tu parte, puedes **ocultar tus mensajes bajo advertencias de contenido**, por ejemplo, si quieres discutir el final de un libro o los sucesos del último episodio de una serie. Y en lugar de condenar completamente tu cuenta a ser “de contenido sensible” como en Twitter, donde queda oculta a todas las personas que hayan marcado la opción de no ver contenido sensible, en Mastodon puedes **ocultar contenido solo en publicaciones específicas** (o, desde luego, configurarlo así por defecto).

También puedes ocultar de tu perfil la lista de personas que sigues y te siguen.

### Herramientas de moderación

Cuando alguien rompe las reglas de la instancia, ya no se trata de ocultar sus contenidos de tu línea personal: aquí interviene la moderación. Mastodon tiene una función para denunciar, donde puedes reportar una cuenta a la administración de la instancia, opcionalmente especificando un mensaje y eligiendo las publicaciones que quieres añadir al informe como ejemplos. Si la persona denunciada pertenece a una instancia diferente, también puedes enviar una copia anónima del reporte a lxs administradorxs de ese servidor, ya que podrían querer saber sobre el caso de un agresor.

El sistema de moderación de Mastodon es muy similar al de un foro. Ofrece dos roles administrativos: moderadorxs y administradorxs. Cuando llega un reporte, ambos grupos reciben una notificación por correo y pueden proceder a:

- Ignorar la denuncia si no tiene fundamentos;
- Añadir observaciones a la cuenta para que lxs otrxs miembrxs del equipo vean;
- Eliminar las publicaciones ofensivas;
- Aislar *(sandbox)* la cuenta para que nadie que no la esté siguiendo pueda ver sus publicaciones;
- Inhabilitar temporalmente el acceso a la cuenta;
- Eliminar definitivamente la cuenta y toda su información.

Más allá de que la cuenta ofensiva esté en tu servidor o uno diferente, estas medidas tienen aplicación dentro de tu instancia, que es la forma de permitir que instancias con diferentes políticas pueden coexistir en la red: tú moderas de acuerdo a las tuyas, yo modero de acuerdo a las mías.

Si hay una instancia radicalmente opositora a la tuya, o una que se rehúsa a controlar a sus agresorxs a tal nivel que moderarlxs individualmente desde tu instancia se vuelve demasiado trabajoso, existen las opciones de aislar o bloquear el dominio completo.

### Conclusión

Donde sea que la gente se reúna, habrá desacuerdos y conflictos. Mantener comunidades donde todxs lxs miembrxs se sientan segurxs no es sencillo. Mastodon proporciona un gran marco de desarrollo y herramientas para hacerlo, y canaliza el poder de aplicar cambios desde una única entidad comercial hacia las comunidades por sí mismas.
