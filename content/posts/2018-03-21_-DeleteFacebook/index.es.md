---
title: "#EliminaFacebook"
subtitle: "Perspectiva desde una plataforma que no pone en riesgo a la democracia"
date: 2018-03-21
draft: false
slug: delete-facebook
author: gargron
resources:
- name: hero
  src: 1_hxrq79FKzukLRoOA8kDiaw.png
categories:
- Op-Ed
tags:
- facebook
- surveillance
---

En el fondo siempre lo supiste. Casi sin percibirlo, siempre escuchaste a la gente que hablaba de la erosión de la privacidad, que no existe el queso gratis, que si no pagas entonces el producto eres tú. Ahora sabes que es cierto. Cambridge Analytica [succionó los datos tan amable y diligentemente recolectados por Facebook y los utilizó para intervenir en procesos electorales en todo el mundo](https://www.integracion-lac.info/es/search/node/cambridge%20analytica?page=1) (y quién sabe para qué más).

Poco importa que el término correcto sea o no “filtración”. **El problema es la cantidad de información que Facebook recolecta, almacena y analiza sobre nosotros**. Ahora sabemos cómo la plataforma de Facebook fue utilizada por terceros para intervenir en procesos electorales. Pero imagina cuánto mayor sería su efectividad si, en lugar de terceros, fuera utilizado por Facebook mismo. Imagina, por ejemplo, [si Mark Zuckerberg decidiera postularse a la presidencia](https://es-us.finanzas.yahoo.com/noticias/mark-zuckerberg-podr%C3%ADa-postularse-la-presidencia-eeuu-en-220700309.html)…

**#DeleteFacebook** es tendencia en Twitter. Y con razón. Algunos señalan que “incluso aunque no tengas una cuenta, Facebook te rastrea en la red y construye un perfil oculto”. Es cierto. ¿Y qué? Hay extensiones para el navegador y aplicaciones que bloquean los dominios de Facebook. **Oblígalos a esforzarse**. No es lo mismo que *entregarles* la información.

Algunos dicen “no quiero dejar de usar Facebook, quiero que Facebook cambie”. Es un error. Mantener el contacto con tus amistades es bueno. **Pero el modelo de negocios y de datos de Facebook es corrupto desde sus fundamentos**. Para ti, tu información es quien eres. Para Facebook, tu información es su capital. Quitártela *es* su único negocio: todo lo demás es pura decoración.

Otros dirán “necesito Facebook porque es donde está mi audiencia, y mi negocio depende de eso”. Es cierto. Pero depender de Facebook no es seguro a largo plazo, [como muchos entendieron por las malas](http://splitsider.com/2018/02/how-facebook-is-killing-comedy/). Algoritmos opacos y en permanente cambio hacen siempre más y más difícil alcanzar a “tu” audiencia. Así que incluso en este caso es recomendable buscar alternativas y tener planes de contingencia.

Puedes mantenerte en contacto con tus amistades sin Facebook. Hay plataformas que no te obligan a venderte al *Big Data* a cambio de un sistema diseñado para producir picos de dopamina con la exactitud perfecta para mantenerte cautivo indefinidamente.

Mastodon es una de esas plataformas, pero no la única. También hay otras como [Diaspora](https://diasporafoundation.org/), [Scuttlebutt](https://www.scuttlebutt.nz/), y [Hubzilla](https://project.hubzilla.org/page/hubzilla/hubzilla-project), pero por razones obvias estoy más familiarizado con Mastodon.

Mastodon no está construido alrededor de la minería de datos. No tiene políticas de nombre real, ni exige fechas de nacimiento, ni registra tus datos de geolocalización. Almacena lo justo y necesario para que puedas hablar e interactuar con tus amistades y seguidorxs. No te rastrea por toda la red. La información que Mastodon almacena es tuya, te pertenece: puedes eliminarla, copiarla, descargarla, compartirla.

Mastodon no tiene que impresionar ni satisfacer inversores, porque no es una red social comercial. Es software abierto, gratuito, de financiamiento colectivo. Sus incentivos están naturalmente alineados con sus usuarixs, por lo que no hay anuncios, ni extraños patrones de funcionamiento. Está allí, creciendo y creciendo: más de 130.000 personas estuvieron activas en Mastodon la semana pasada.

Para producir un impacto, debemos actuar. Es tentador esperar a que otrxs hagan el cambio, porque, ¿y si lxs demás no nos siguen? Pero las acciones individuales definitivamente suman. Una de mis historias favoritas es la de alguien a quien le solicitaron su nombre de usuarix en redes sociales para una conferencia de desarrolladorxs de videojuegos y al responder con el de Mastodon recibió asentimientos comprensivos en lugar de miradas de confusión. Paso a paso, con cada nueva persona, cambiarse a Mastodon se volverá más y más fácil.

Ahora es tiempo de actuar. [Únete a Mastodon hoy](https://joinmastodon.org).
