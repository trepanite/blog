---
title: "No es sobre las redes sociales. Es sobre el control."
date: 2018-02-19
draft: false
author: mainebot
resources:
- name: hero
  src: 1_6jfDdmdbGDIheNvg2mFbvg.jpeg
categories:
- Op-Ed
tags:
- mastodon
- amazon
- facebook
- twitter
---

Lxs seres humanxs somos, sobre todo, contadorxs de historias. Es nuestra forma de relacionarnos con nuestro propio pasado, con nuestras personalidades, y con lxs demás. Es como nos conectamos con el mundo a nuestro alrededor, como comprendemos lo que sucede, y como apreciamos los valores. Confiamos en nuestros relatos para funcionar como entes en el mundo.

Generalmente, estas historias se relatan en persona: “hoy hice esto y me sentí así y entonces sucedió aquello y fue como ¡increíble, no puede ser!”. En épocas anteriores nos apoyábamos en la tradición oral para transmitir valores. Luego nos fuimos desplazando hacia la forma escrita, a medida que la literatura crecía y las ciudades se convertían en el principal entorno donde la gente habitaba.

Las redes sociales fueron, en cierto sentido, una manera de incorporar la tradición oral al mundo escrito: el transcurso de nuestra historia personal en vivo, a medida que iba ocurriendo, registrado de la forma que mejor creíamos que encajaría en nuestra línea narrativa. Podíamos diferir en los detalles, porque los detalles no eran tan importantes para la historia como el hecho de contarla.

Pero ahora la historia en sí misma es contada por nosotros.

Nuestros relatos, nuestro sentido de pertenencia a las narrativas que creamos, está sometido a revisión constante. Facebook y Twitter nos quitaron la continuidad lineal de la historia. No vemos nuestra participación. Vemos nuestras contribuciones. Y solo si son lo suficientemente populares entre nuestrxs pares (y por lo tanto, entre los anunciantes) para generar tráfico.

Lo que obtenemos es un barniz, un resumen de nuestra historia personal, intercalado con publicaciones que ni escribimos ni tienen relación con nosotrxs. Nuestra historia y nuestros intereses, la manera específica que tenemos de autodefinirnos, es intercalada con elementos de lo que se ha determinado que es lucrativo, y por lo tanto interesante, para maximizar nuestra participación en una falsa narración de la historia.

Hemos perdido la responsabilidad que nos correspondía al relatar las historias que definen quiénes somos. Paso a paso, nuestro control sobre nuestra identidad fue erosionado, monetizado y comercializado.

Antes de continuar, quiero hablar de ‘The Self Driven Child’; un libro reseñado recientemente en [Scientific American](https://www.scientificamerican.com/article/the-case-for-the-ldquo-self-driven-child-rdquo/) (en inglés). Es una lectura interesante, pero la moraleja importante es esta: la pérdida gradual de autonomía en las vidas de niñxs y adolescentes condujo a un aumento significativo en los índices de ansiedad y depresión, un incremento general de los casos de problemas de salud mental y empeorado el sueño.

Esto es más que la simple remoción de la elección de actividades: nuestras fuentes de distracción aumentaron, la libertad de de elección de muchxs fue reemplazada por deberes hogareños y actividades estructuradas sobre la responsabilidad y el tiempo libre.

Todos los espacios en línea, pero particularmente Facebook y Twitter, fueron reemplazando lentamente la independencia del participantx por un facsímil estructurado, ludificado y restrictivo. Llevan una década haciéndolo. De hecho, llevan tanto tiempo haciéndolo que para muchas personas nunca hubo una versión de esos espacios que no estuviera absolutamente fuera de sus manos.

Con el reemplazo de la línea de tiempo cronológica por sus propios algoritmos de ordenamiento, nos quitaron por completo el control sobre la conversación. En lugar de estar hablando e interactuando con tus amistades, estás hablando de las fuentes de contenido que se te indica que veas, leas y disfrutes. Estás en competencia directa con una noción corporativa de tu historia personal, tu identidad y tus relaciones.

Esto se puede ver más claramente en Instagram: una cronología repleta de contenido patrocinado de desconocidos, anuncios que no se pueden distinguir de las publicaciones reales, “sugerencias” e incluso inserciones arbitrarias en tu historial de búsqueda y etiquetas. Es más, este no es un caso donde recibes sugerencias sobre "cosas que te podrían gustar”. Esto es que te digan, explícitamente, lo que te gusta. Tu historia ya no es un registro ni un relato; es un asunto de revisión corporativa bajo estricta auditoría.

“Pero somos libres de participar, o no, en esas plataformas”, podrías responder. Y es cierto, sí; podemos tomar el balón y marcharnos a casa.

Pero no lo haremos, ni tendríamos por qué hacerlo.

Es ingenuo creer que deberíamos dejar de participar en el discurso público porque no estamos satisfechos con el paradigma actual de las redes sociales. Para mucha, mucha gente en el mundo de hoy, acceder a la comunicación y discutir con personas de todo del mundo más allá de las fronteras y las ideologías, de las políticas y las religiones, más que un lujo es la norma. Sugerir que sencillamente no se participe en las redes sociales es tan poco realista como sugerir no pagar por la electricidad, abandonar internet o cancelar tu teléfono. Se puede hacer, sí, seguro, pero para la mayoría de nosotrxs sería francamente ridículo.

Necesitamos una plataforma que no esté sujeta a intereses corporativos, que no esté orientada a la recolección y comercialización de información personal, ni a la monetización de la experiencia social mediante agresivas estrategias de despersonalización, ni al servicio publicitario.

Por estas razones necesitamos Mastodon.

*¡Para empezar con Mastodon, visita [JoinMastodon.org](https://joinmastodon.org/) y escoge un lugar al que llamar hogar! ¡Usa los menús desplegables para afinar tu búsqueda por intereses e idioma, y comienza a tootear!*
