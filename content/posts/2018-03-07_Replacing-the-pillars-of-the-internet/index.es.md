---
title: "Reemplazando los pilares de internet"
subtitle: "Un breve repaso a los esfuerzos e innovaciones actuales del movimiento por la descentralización de la World Wide Web."
date: 2018-03-07
draft: false
author: mainebot
resources:
- name: hero
  src: 1_1P5qRd7pWr2Xse7-VuIoNg.jpeg
categories:
- Op-Ed
tags:
- mastodon
- amazon
- facebook
- twitter
---

*Este artículo asume que has leído mis dos entradas previas, [aquí]({{< ref "posts/2018-02-19_This-isnt-about-social-media-this-is-about-control/index.es.md" >}}) y [aquí]({{< ref "posts/2018-02-28_The-centralization-of-power-on-the-internet/index.es.md" >}}). ¿Por qué no darles una leída rápida si aún no lo hiciste?*

Para ponderar el nivel de miseria y desolación de un internet absolutamente secuestrado por los intereses corporativos y servido a través de canales únicos sin la menor posibilidad de competencia, merece la pena que seamos conscientes de cuan penetrante y poderosa es la idea de la descentralización en el siglo XXI.

La estructura actual de las cosas es un remanente de como siempre se hicieron las cosas: confiamos en una autoridad particular que administre todo tras bambalinas para que nuestra experiencia de este lado permanezca impecable. Los proveedores de internet (ISPs), alguna vez un requisito fundamental, se están volviendo caducos, anticuados e innecesarios. ¿Necesitamos un intermediario para administrar lo que a todos los fines y propósitos no es más que el acceso a una herramienta?

Ahora bien, lo mismo vale para los actos de comunicación en sí mismos. No necesitamos servidores centralizados, propiedad ostensible de una sola organización: vivimos en una época donde las computadoras que llevamos con nosotros, las que tenemos en nuestros escritorios e incluso las que corremos como instancias virtualizadas en la nube tienen la potencia suficiente para satisfacer los mismos fines, sin la necesidad de que una entidad impulsada por la obtención y acumulación de ganancias deba hacer el trabajo duro por nosotros.

La descentralización de los servicios en internet es clave. Ya transformó en sus fundamentos la manera de compartir grandes archivos en línea: bittorent es, sin importar la opinión que tengas al respecto, una demostración enormemente exitosa del poder de los servicios descentralizados.

Mastodon es más que solo una plataforma parecida a Twitter. Es la prueba de que el microblogging no tiene que ser propiedad de una corporación para ser funcional. Más aún, es flexible: con apenas algunas modificaciones, las instancias de Mastodon pueden operar como Instagram, como Snapchat, o como cualquier otro contenido que venga apilado en un contenedor.

Más que la similitud en su funcionalidad, puede mantener la intercompatibilidad y seguir ampliando la federación a instancias que funcionan con reglas absolutamente diferentes. Un proyecto, Peertube, hace exactamente esto. Es una plataforma para compartir videos que utiliza el mismo sistema que Mastodon, federado y descentralizado, solo que se orienta a los clips de video.

Fuera de las redes sociales, la descentralización allana –y allanó– el camino hacia la comunicación radical. No solemos considerarlo con frecuencia, pero la World Wide Web en sí misma es descentralizada (o debería serlo, a no ser que le preguntes a Facebook), y también lo es el correo electrónico: el sistema de comunicaciones federado primigenio. Proyectadas hacia el futuro, esas ideas cobran nueva vida.

{{< figure src="1_hEV04QagABCWErJl47SRHQ.jpeg" >}}

[Matrix](https://matrix.org/) es exactamente la clase de desarrollo apasionante que esperábamos para las comunicaciones de alta velocidad, sincrónicas, y más. Ofrece comunicaciones extremadamente seguras de extremo a extremo, está diseñado para aplicarlo en casi cualquier canal de comunicación y listo para que desarrolladores corporativos lo implemente. No mañana, sino ahora: puedes empezar a usar Matrix de inmediato.

Este es un desarrollo serio: tanto que el teléfono Librem5 de [Purism](https://puri.sm/shop/librem-5/) lo incorpora de forma nativa.

Pensemos en el futuro, pensando en el presente.

Pese al actual clima político en Estados Unidos, muchos estados consagran reglas de neutralidad en la red destinadas a impedir que los proveedores de internet practiquen el favoritismo con el tráfico. Esto es, en mi opinión, un paso importante para asegurar que operen como utilidades y no como lujos.

Pero, ¿son realmente necesarios los proveedores? Muchas comunidades demandaron a los mayores ISPs por incumplir contratos y en su lugar optaron por instalar y administrar redes de fibra óptica de ultra alta velocidad ellas mismas. En Nueva York, esto ha ido un paso más allá: [NYMesh](https://nycmesh.net/).

{{< figure src="1_vLtFVPTHJGDfw3XOl4C1Sw.jpeg" >}}

Una red descentralizada, de alta velocidad, que opera nodo a nodo, independiente de los proveedores y sin fines de lucro. No solo es de propiedad social comunitaria y orientada al uso público; además es funcional incluso en casos de emergencias, para quien sea que desee participar. Las velocidades que proporciona son comparables, y superan, a las que se puede obtener de un proveedor tradicional a precios razonables.

A medida que las mejoras de hardware posibilitan la trasmisión de datos inmediata y que las redes *mesh* puedan operar teléfono a teléfono, o incluso desde repetidores inalámbricos locales de propiedad y responsabilidad a nivel municipal, la necesidad de estructuras corporativas que existan como mecanismos de control desaparece por completo. Nos encontramos al borde de un cambio masivo hacia el final del control central de nuestras experiencias, pero solo si estamos dispuestos a hacer los cambios nosotrxs mismxs.

Mastodon no es el primer lo-que-sea descentralizado, pero es la primera prueba real de que podemos tener lo que hasta hace poco tiempo solo eran promesas de las grandes corporaciones a cambio de nuestra privacidad, nuestros datos y nuesta libertad intelectual. Reapropia una plataforma; la primera de muchas.

¿Cuánto tiempo pasará hasta que alguien desarrolle una forma de alojar una plataforma como Facebook sin la necesidad de un servidor centralizado? No tiene que ser mucho: ya tenemos los medios hoy, lo único que necesitamos ahora es la voluntad de cambiar.

*¡Para empezar con Mastodon, visita [JoinMastodon.org](https://joinmastodon.org/) y escoge un lugar al que llamar hogar! ¡Usa los menús desplegables para afinar tu búsqueda por intereses e idioma, y encuentra una comunidad que sientas como propia! ¡No dejes que el fediverso se pierda lo que tienes para decir!*
